package com.example.tp2;

import androidx.appcompat.app.AppCompatActivity;
import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import static android.content.ContentValues.TAG;

public class AuthActivity extends AppCompatActivity {

    String result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.auth_activity);
    }
    protected void onStart() {
        super.onStart();
        Button b = (Button)findViewById(R.id.authentificate);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EditText log = (EditText)findViewById(R.id.login);
                EditText pass = (EditText)findViewById(R.id.password);
                String username = log.getText().toString();
                String password = pass.getText().toString();

                Thread thread = new Thread(new Runnable() {

                    @Override
                    public void run() {
                        try  {
                            URL url = null;
                            try {
                                url = new URL("https://httpbin.org/basic-auth/bob/sympa");
                                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                                String basicAuth = "Basic " + Base64.encodeToString((username+":"+password).getBytes(),
                                        Base64.NO_WRAP);
                                urlConnection.setRequestProperty ("Authorization", basicAuth);

                                try {
                                    InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                                    String s = readStream(in);
                                    JSONObject jsonResult = new JSONObject(s);
                                    String res = jsonResult.getString("authenticated");
                                    result=res;
                                    Log.i("JFL", result);
                                } finally {
                                    urlConnection.disconnect();
                                }
                            } catch (MalformedURLException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                                result = "false";
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                TextView authResult = (TextView)findViewById(R.id.result);
                                authResult.setText(result);
                            }
                        });
                    }
                });

                thread.start();
            }

            private String readStream(InputStream in) {
                BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                StringBuilder sb = new StringBuilder();

                String line = null;
                try {
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                } catch (IOException e) {
                    Log.e(TAG, "IOException", e);
                } finally {
                    try {
                        in.close();
                    } catch (IOException e) {
                        Log.e(TAG, "IOException", e);
                    }
                }
                return sb.toString();
            }
        });
    }
}

